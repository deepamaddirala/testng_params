package TestNG_Params;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class Env_Test {

	@Test
	@Parameters("env")
	public void method1(String env) {
		switch (env) {
		case "dev": {
			System.out.println("this is running in dev environment");
			break;
		}
		case "prod": {
			System.out.println("this is running in production environment");
			break;
		}
		default:
			System.out.println("Invalid environment details given");

		}

	}
}
